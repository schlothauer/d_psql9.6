#!/bin/bash


scriptPos=${0%/*}

source "$scriptPos/conf.sh"

docker ps -f name="$CONTAINER_NAME" | grep "$CONTAINER_NAME" > /dev/null && echo -en "\033[1;31m  the container already runs: $CONTAINER_NAME\033[0m\n" && exit 1

aktImgName=`docker images |  grep -G "$IMAGE_BASE *$IMAGE_VERS *" | awk '{print $1}'`
aktImgVers=`docker images |  grep -G "$IMAGE_BASE *$IMAGE_VERS *" | awk '{print $2}'`

if [ "$aktImgName" == "$IMAGE_BASE" ] && [ "$aktImgVers" == "$IMAGE_VERS" ]
then
        echo "run container from image: $IMAGE"
else
        if docker build -t $IMAGE $scriptPos/../../image
    then
        echo -en "\033[1;34m  image created: $imageName \033[0m\n"
    else
        echo -en "\033[1;31m  error while build image: $imageName \033[0m\n"
        exit 1
    fi
fi

# this directory is needed because it contains the postgres tuning scripts
extConfDir=$scriptPos/../conf
extConfDir=`pushd $extConfDir > /dev/null; pwd ; popd > /dev/null`

dataDir=$scriptPos/../data
if ! [ -d $dataDir ]; then
    mkdir -p $dataDir
fi
dataDir=`pushd $dataDir > /dev/null; pwd ; popd > /dev/null`


if docker ps -a -f name="$CONTAINER_NAME" | grep "$CONTAINER_NAME" > /dev/null; then
    docker start $CONTAINER_NAME
else
    docker run -d --name "$CONTAINER_NAME" -v ${extConfDir}:/opt/extra-conf -v ${dataDir}:/opt/pgdata -e CONFIG_PREPARE_FILE=/opt/extra-conf/tune_postgres.sh -e POSTGRES_PASSWORD="demo_password" -e POSTGRES_USER="demo" -e POSTGRES_DB="demo_db" -e PGDATA=/opt/pgdata "$IMAGE"
fi



